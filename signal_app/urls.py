from django.urls import path
from signal_app import views

app_name = 'signal_app'
urlpatterns = [
    path('', views.home),
    path('contact/', views.contact),
]

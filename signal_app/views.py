from django.shortcuts import render
from datetime import datetime, timedelta
from django.core.signals import request_finished
from django.dispatch import receiver, Signal
from django.http import HttpResponse
from .models import Post

# Create your views here.

# custom signal
request_counter_signal = Signal(providing_args=["current_time"])


def home(request):
    request_counter_signal.send(sender=Post, current_time=datetime.now())
    return HttpResponse("Hey guys this is a home page !!!!!")


@receiver(request_finished)
def post_request_receiver(sender, **kwargs):
    print("Request finished!")


def contact(request):
    return HttpResponse("Hey guys this is contact us page!!!")


@receiver(request_counter_signal)
def request_counter_signal_receiver(sender, **kwargs):
    print("Customized signal !!!!! ",kwargs['current_time'])

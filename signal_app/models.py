from django.db import models
from django.db.models.signals import pre_save, post_save
from django.dispatch import receiver


# Create your models here.

class Post(models.Model):
    title = models.CharField(max_length=15)

    def __str__(self):
        return self.title


# def save_post(sender, **kwargs):
#     print("post has been saved!!!!!!!!!!!")
#
#
# post_save.connect(save_post, sender=Post, weak=False)

# second way of registering the receiver using decorator

@receiver(post_save, sender=Post)
def save_post(sender, **kwargs):
    print("post has been saved!!!!!!!!!!!")